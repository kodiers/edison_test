import redis
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.views.decorators.http import require_POST
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.conf import settings

from common.decorators import ajax_required

r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

# Create your views here.


class UserRegistrationView(CreateView):
    """
    User registration class based views.
    """
    template_name = "account/register.html"
    form_class = UserCreationForm
    success_url = reverse_lazy('index_view')

    def form_valid(self, form):
        """
        Validate user forms
        :param form: UserCreationForm instance
        :return: HttpResponse (redirect to success_url)
        """
        result = super(UserRegistrationView, self).form_valid(form)
        cd = form.cleaned_data
        user = authenticate(username=cd['username'], password=cd['password1'])
        login(self.request, user)
        return result


@require_POST
@ajax_required
def get_username_by_id(request):
    """
    Return username by user id (if found).
    :param request: HttpRequest (Post, AJAX)
    :return: HttpResponse (Json)
    """
    try:
        user = User.objects.get(pk=request.POST.get('pk'))
        return JsonResponse({"status": "ok", "username": user.username})
    except User.DoesNotExist:
        return JsonResponse({"status": "error", "reason": "not found"})