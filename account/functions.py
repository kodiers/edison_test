from django.contrib.auth.models import User


def get_online_users(r):
    """
    Get online records from redis
    :param r: redis connection object
    :return: QuerySet (Users)
    """
    online_keys = r.keys(pattern="online_*")
    online_id_list = [int(str(key, 'utf-8').split('_')[1]) for key in online_keys]
    users = User.objects.filter(id__in=online_id_list)
    return users