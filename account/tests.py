from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.


class OnlineTestKeys(TestCase):
    def setUp(self):
        User.objects.create_user('user1', 'user1@mail.mm', 'p@ssw0rd')
        User.objects.create_user('user2', 'user2@mail.mm', 'p@ssw0rd')

    def test_online(self):
        self.client.login(username='user1', password='p@ssw0rd')
        self.client.login(username='user2', password='p@ssw0rd')
        response = self.client.get('/api/online-users/')
        self.assertContains(response, 'user1')
        self.assertContains(response, 'user2')
