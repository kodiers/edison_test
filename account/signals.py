import datetime
import json
import redis

from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.conf import settings

r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)


@receiver(user_logged_in)
def set_online_redis_key(sender, user, request, **kwargs):
    """
    Set redis key, than user login
    """
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    online_key = "online_" + str(user.pk)
    r.set(online_key, now)
    r.publish("online_users", json.dumps({"user": user.pk, "time": now, "status": "online"}))


@receiver(user_logged_out)
def set_offline_redis_key(sender, user, request, **kwargs):
    """
    Delete redis key, than user logout
    """
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    online_key = "online_" + str(request.user.pk)
    r.delete(online_key)
    r.publish("online_users", json.dumps({"user": request.user.pk, "time": now, "status": "offline"}))
