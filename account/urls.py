from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^register/$', views.UserRegistrationView.as_view(), name='register'),
    url(r'^get_username_by_id/$', views.get_username_by_id, name='get_username_by_id'),
]