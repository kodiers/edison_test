import redis
import json
import datetime
import tornadoredis
import tornado.web
import tornado.websocket
import tornado.gen
import tornado.ioloop
import tornado.httpserver
import tornado.httpclient
import tornado.options
from tornado.options import define, options

from urllib import parse

from edison_test.settings import API_KEY, REDIS_HOST, REDIS_PORT, REDIS_DB, API_LEAVE_GAME_URL

define("port", default=8888, help="run on given port", type=int)


CLIENTS = []
client = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


class GameHandler(tornado.websocket.WebSocketHandler):
    """
    Game handler class
    """
    def open(self, game_id, user_id):
        """
        When websocket connection is open - listen redis channel for new messages
        :param game_id: int Game ID
        :return: None
        """
        self.game_id = game_id
        self.user_id = user_id
        self.channel = "game_" + str(game_id)
        self.listen()

    @tornado.gen.engine
    def listen(self):
        """
        Connect to redis and subscribe to channel
        :return: None
        """
        self.client = tornadoredis.Client(host=REDIS_HOST, port=REDIS_PORT, selected_db=REDIS_DB)
        self.client.connect()
        yield tornado.gen.Task(self.client.subscribe, self.channel)
        self.client.listen(self.redis_message)

    def on_close(self):
        """
        Unsubscribe on exit
        :return: None
        """
        if self.client.subscribed:
            self.client.unsubscribe(self.channel)
            self.client.disconnect()
        http_client = tornado.httpclient.AsyncHTTPClient()
        request = tornado.httpclient.HTTPRequest(
            API_LEAVE_GAME_URL,
            method='POST',
            body=parse.urlencode({
                "API_KEY": API_KEY,
                "user_pk": self.user_id,
                "game_pk": self.game_id,
            })
        )
        response = http_client.fetch(request, self.handle_request)

    def redis_message(self, message):
        """
        Write published message from redis
        :param message: Message object (dict)
        :return: None
        """
        self.write_message(str(message.body))

    def handle_request(self, response):
        print(response.body)

    def on_message(self, message):
        """
        Handle message from websocket connection
        :param message: json
        :return: None
        """
        raise NotImplementedError("Should implement on_message handler")

    def check_origin(self, origin):
        return True


class OnlineHandler(tornado.websocket.WebSocketHandler):
    """
    Online user handler class
    """
    def open(self, user_id=None):
        """
        When websocket connection is open - listen redis channel for new messages
        :param game_id: int Game ID
        :return: None
        """
        self.channel = "online_users"
        self.user_id = user_id
        self.listen()

    @tornado.gen.engine
    def listen(self):
        """
        Connect to redis and subscribe to channel. Send message when users online
        :return: None
        """
        self.client = tornadoredis.Client(host=REDIS_HOST, port=REDIS_PORT, selected_db=REDIS_DB)
        self.client.connect()
        if self.user_id:
            now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            online_key = "online_" + str(self.user_id)
            record = client.get(online_key)
            if not record:
                client.set(online_key, now)
                self.client.publish(self.channel, json.dumps({"user": self.user_id, "time": now, "status": "online"}))
        yield tornado.gen.Task(self.client.subscribe, self.channel)
        self.client.listen(self.redis_message)

    def on_close(self):
        """
        Unsubscribe on exit. Publish message when user offline.
        :return: None
        """
        if hasattr(self, 'user_id') and self.user_id:
            self.client.publish(self.channel, json.dumps({"user": self.user_id,
                                                     "time": datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
                                                     "status": "offline"}))
            online_key = "online_" + str(self.user_id)
            online_record = client.get(online_key)
            if online_record:
                client.delete(online_key)
        if self.client.subscribed:
            self.client.unsubscribe(self.channel)
            self.client.disconnect()

    def redis_message(self, message):
        """
        Write published message from redis
        :param message: Message object (dict)
        :return: None
        """
        self.write_message(str(message.body))

    def on_message(self, message):
        """
        Handle message from websocket connection
        :param message: json
        :return: None
        """
        raise NotImplementedError("Should implement on_message handler")

    def check_origin(self, origin):
        return True


class NewGameHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        """
        When websocket connection is open - listen redis channel for new messages
        :param game_id: int Game ID
        :return: None
        """
        self.channel = "new_games"
        self.listen()

    @tornado.gen.engine
    def listen(self):
        """
        Connect to redis and subscribe to channel. Send message when users online
        :return: None
        """
        self.client = tornadoredis.Client(host=REDIS_HOST, port=REDIS_PORT, selected_db=REDIS_DB)
        self.client.connect()
        yield tornado.gen.Task(self.client.subscribe, self.channel)
        self.client.listen(self.redis_message)

    def on_close(self):
        """
        Unsubscribe on exit. Publish message when user offline.
        :return: None
        """
        if self.client.subscribed:
            self.client.unsubscribe(self.channel)
            self.client.disconnect()

    def redis_message(self, message):
        """
        Write published message from redis
        :param message: Message object (dict)
        :return: None
        """
        self.write_message(str(message.body))

    def on_message(self, message):
        """
        Handle message from websocket connection
        :param message: json
        :return: None
        """
        raise NotImplementedError("Should implement on_message handler")

    def check_origin(self, origin):
        return True


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[
            (r'/game_start/(?P<game_id>\d+)/(?P<user_id>\d+)/', GameHandler),
            (r'/check_users/(?P<user_id>\d+)/', OnlineHandler),
            (r'/check_users/', OnlineHandler),
            (r'/check_games/', NewGameHandler),
        ]
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
