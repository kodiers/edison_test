import redis
import json
import datetime
import random

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.views.decorators.http import require_POST
from django.http import JsonResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from common.decorators import ajax_required

from . import constants
from .forms import GameLobbieForm
from .models import GameLobbie, Party, Round
from .functions import publish_round_information

r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

# Create your views here.


def index_view(request):
    """
    Show index page. Show games list and online users list.
    :param request: HttpRequest
    :return: HttpResponse
    """
    games = GameLobbie.objects.all()
    return render(request, "game/index.html", {"games": games})


@login_required
def create_game(request):
    """
    Handle create game form. If game created redirect to game.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == 'POST':
        form = GameLobbieForm(request.POST)
        if form.is_valid():
            new_game = form.save(commit=False)
            new_game.author = request.user
            new_game.save()
            game_round = Round()
            game_round.game = new_game
            game_round.save()
            r.publish('new_games', json.dumps({'title': new_game.title, 'author': new_game.author.username,
                                               'pk': new_game.pk, 'game_status': 'Participants set', 'status': 'new'}))
            return redirect(reverse_lazy('game:join_game', args=[new_game.pk]))
    else:
        form = GameLobbieForm()
    return render(request, "game/create_game.html", {"form": form})


@login_required
def join_game(request, pk):
    """
    Join game.
    :param request: HttpRequest
    :param pk: int: GameLobbie pk
    :return: HttpResponse
    """
    game = get_object_or_404(GameLobbie, pk=pk)
    user_leaved_key = "leaved_" + str(request.user.pk)
    # Check if user leaved previous his game
    record = r.get(user_leaved_key)
    if record:
        leaved_date = datetime.datetime.strptime(str(record, 'utf-8'), "%Y-%m-%d %H:%M")
        delta = datetime.datetime.now() - leaved_date
        if delta.total_seconds() < 60.0:
            messages.info(request, "You should wait 1 minute before join game")
            return redirect(reverse_lazy('index_view'))
        else:
            r.delete(user_leaved_key)
    if (request.user != game.author or request.user != game.opponent) and game.opponent is not None:
            messages.error(request, "All players joined the game")
            return redirect(reverse_lazy('index_view'))
    choices = Party.GAME_CHOICES
    channel = "game_" + str(game.pk)
    r.publish(channel, json.dumps({"game_id": game.pk}))
    if game.status == constants.PARTICIPANTS_SET and not game.opponent:
        if request.user != game.author:
            game.opponent = request.user
            game.status = constants.READY_FOR_GAME
            game.save()
            r.publish(channel, json.dumps({"game_status": "ready", "opponent": request.user.username}))
    else:
        if request.user != game.author and request.user != game.opponent:
            messages.error(request, "You are already joined this game")
            return redirect(reverse_lazy("index_view"))
    game_rounds = game.game_rounds.all()
    last_round = game_rounds.latest('created')
    return render(request, "game/playing_game.html", {"game": game, 'choices': choices, "game_rounds": game_rounds,
                                                      "last_round": last_round})


@login_required
@require_POST
@ajax_required
def start_game(request):
    """
    Change game status to GAME_STARTED
    :param request: HttpRequest (Post, ajax)
    :return: HttpResponse (Json)
    """
    try:
        game = GameLobbie.objects.get(pk=request.POST.get('pk'))
        game.status = constants.GAME_STARTED
        game.save()
        game_round = game.game_rounds.last()
        channel_name = "game_" + str(game.pk)
        r.publish(channel_name, json.dumps({"round": game_round.round_number, "round_id": game_round.pk,
                                            "game_status": "started"}))
        return JsonResponse({"status": "ok", "round": game_round.round_number})
    except GameLobbie.DoesNotExist:
        return JsonResponse({"status": "error", "text": "Game not found"})


@login_required
@require_POST
@ajax_required
def make_party(request):
    """
    Make user party
    :param request: HttpRequest (Post, Ajax)
    :return: HttpResponse (Json)
    """
    game = get_object_or_404(GameLobbie, pk=request.POST.get('pk'))
    game_round = Round.objects.get(pk=request.POST.get("round"))
    if game_round.parties.count() > 2:
        return JsonResponse({"error": "all users make party"})
    if Party.objects.filter(round=game_round, user=request.user).exists():
        return JsonResponse({"error": "you make you party"})
    party = Party()
    party.game = game
    party.user = request.user
    party.user_choice = request.POST.get("choice")
    party.round = game_round
    party.save()
    channel_name = "game_" + str(game.pk)
    r.publish(channel_name, json.dumps({"round_id": game_round.pk, "round_state": "make_party",
                                        "player": request.user.username}))
    # if all users make party calculate winner
    if game_round.parties.count() == 2:
        party_1 = game_round.parties.all()[0]
        party_2 = game_round.parties.all()[1]
        winner = None
        winner_pk = None
        if (party_1.user_choice + 1) % 5 == party_2.user_choice:
            game_round.winner = party_2.user
            winner = party_2.user.username
            winner_pk = party_2.user.pk
        elif (party_1.user_choice + 2) % 5 == party_2.user_choice:
            game_round.winner = party_2.user
            winner = party_2.user.username
            winner_pk = party_2.user.pk
        elif party_1.user_choice == party_2.user_choice:
            game_round.is_tie = True
        else:
            game_round.winner = party_1.user
            winner = party_1.user.username
            winner_pk = party_1.user.pk
        game_round.save()
        if game.calculate_game_winner(r, channel_name, game_round):
            return JsonResponse({"status": "ok", "game status": "completed"})
        publish_round_information(r, channel_name, game_round.game, game_round, False, winner, winner_pk)
    return JsonResponse({"status": "ok", "round": game_round.round_number})


@csrf_exempt
@require_POST
def leave_game(request):
    """
    If user leaves game ake record in redis
    :param request: HttpRequest (Post from tornado)
    :return: HttpResponse
    """
    api_key = request.POST.get("API_KEY")
    if api_key == settings.API_KEY:
        try:
            user = User.objects.get(pk=request.POST["user_pk"])
        except User.DoesNotExist:
            return JsonResponse({"status": "error", "reason": "User not found"})
        try:
            game = GameLobbie.objects.get(pk=request.POST["game_pk"])
        except GameLobbie.DoesNotExist:
            return JsonResponse({"status": "error", "reason": "Game not found"})
        if game.status != constants.GAME_ENDED:
            game.status = constants.GAME_ENDED
            if game.author == user:
                game.game_winner = game.opponent
            else:
                game.game_winner = game.author
            user_leaved_key = "leaved_" + str(user.pk)
            r.set(user_leaved_key, datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
            game.save()
            return JsonResponse({"status": "ok", "reason":"game ended"})
        return JsonResponse({"status": "ok", "reason": "game ended normaly"})
    else:
        return JsonResponse({"status": "error", "reason": "API KEY incorrect"})


@require_POST
@ajax_required
@login_required
def get_hint(request):
    """
    Return user hint
    :param request: HttpRequest (POST, AJAX)
    :return: HttpResponse (JSON)
    """
    game_round = Round.objects.get(pk=request.POST.get('round'))
    if request.user in game_round.game.users_hint_used.all():
        return JsonResponse({"error": "You used hint"})
    parties = game_round.parties.exclude(user=request.user)
    random_hint = random.choice([constants.ROCK, constants.PAPER, constants.SCISSORS, constants.SPOCK,
                                 constants.LIZARD])
    game_round.game.users_hint_used.add(request.user)
    game_round.user_hint = request.user
    game_round.save()
    if parties.exists():
        party_choice = parties[0].user_choice
        hint = random.choice([party_choice, random_hint])
    else:
        return JsonResponse({"error": "You loose hint"})
    if hint == constants.ROCK:
        hint_str = "rock"
    elif hint == constants.PAPER:
        hint_str = "paper"
    elif hint == constants.SCISSORS:
        hint_str = "scissors"
    elif hint == constants.SPOCK:
        hint_str = "Spock"
    else:
        hint_str = "lizard"
    channel_name = "game_" + str(game_round.game.pk)
    username = request.user.username
    r.publish(channel_name, json.dumps({'message': 'User: {} use hint'.format(username), 'hint': hint_str,
                                        'game_status': 'hint_used'}))
    return JsonResponse({"status": "ok", "hint": hint_str})


@require_POST
@ajax_required
@login_required
def round_timeout(request):
    """
    End round by timeout
    :param request: HttpRequest(POST, AJAX)
    :return: HttpResponse
    """
    game_round = Round.objects.get(pk=request.POST.get('pk'))
    channel_name = "game_" + str(game_round.game.pk)
    parties = game_round.parties.all()
    if parties.count() == 1:
        game_round.winner = parties[0].user
    else:
        game_round.is_tie = True
    game_round.save()
    game = game_round.game
    if game.calculate_game_winner(r, channel_name, game_round):
        return JsonResponse({"status": "ok", "game status": "completed"})
    publish_round_information(r, channel_name, game_round.game, game_round, True)
    return JsonResponse({"status": "ok"})
