from django import template


register = template.Library()


@register.filter()
def add_class_to_formfield(field, css):
    """
    Added css class to form field.
    """
    return field.as_widget(attrs={"class": css})