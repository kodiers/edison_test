from . import constants


def game_constants(request):
    """
    Add constants to request.
    """
    return {"PARTICIPANTS_SET": constants.PARTICIPANTS_SET, "READY_FOR_GAME": constants.READY_FOR_GAME,
            "GAME_STARTED": constants.GAME_STARTED, "GAME_ENDED": constants.GAME_ENDED}