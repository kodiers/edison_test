from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^online-users/$', views.OnlineUsersAPIView.as_view(), name='online_users'),
]