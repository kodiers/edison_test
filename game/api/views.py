import redis

from django.conf import settings

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from account.functions import get_online_users
from .serializers import UserSerializer

r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)


class OnlineUsersAPIView(ListAPIView):
    """
    Return online users list
    """
    permission_classes = (IsAuthenticatedOrReadOnly, )

    def get_queryset(self):
        users = get_online_users(r)
        return users

    def list(self, request, *args, **kwargs):
        serializer = UserSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)
