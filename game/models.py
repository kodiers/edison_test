import json
from django.http import JsonResponse
from django.db import models
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User

from . import constants
# Create your models here.


class BaseModel(models.Model):
    """
    Model for add created/updated fields
    """
    created = models.DateTimeField(auto_now_add=True, verbose_name="Created")
    updated = models.DateTimeField(auto_now=True, verbose_name="Updated")

    class Meta:
        abstract = True


class GameLobbie(BaseModel):
    """
    GameLobbies model
    """
    GAME_STATUS = (
        (constants.PARTICIPANTS_SET, "Participants set"),
        (constants.READY_FOR_GAME, "Ready for game"),
        (constants.GAME_STARTED, "Game started"),
        (constants.GAME_ENDED, "Game ended")
    )

    status = models.PositiveIntegerField(choices=GAME_STATUS, default=0, verbose_name="Status")
    title = models.CharField(max_length=200, verbose_name="Title")
    party_time = models.PositiveIntegerField(default=5, verbose_name="Party time")
    author = models.ForeignKey(User, related_name="my_lobbies", verbose_name="Author")
    opponent = models.ForeignKey(User, related_name="my_games", verbose_name="Opponent", null=True, blank=True)
    game_winner = models.ForeignKey(User, related_name="won_games", verbose_name="Winner", null=True, blank=True)
    users_hint_used = models.ManyToManyField(User, related_name='used_hints', verbose_name="Hints used",
                                       null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("game:join_game", args=[self.pk])

    def calculate_game_winner(self, r, channel_name, game_round):
        """
        Calculate game winner
        :param r: redis connection object
        :param channel_name: str - channel to publish
        :param game_round: Round - current round
        :return: bool: If game ended - return True
        """
        won_rounds = self.game_rounds.filter(is_tie=False)
        # define game winner
        if won_rounds.count() >= 3:
            if won_rounds.filter(winner=self.author).count() > won_rounds.filter(winner=self.opponent).count():
                winner = self.author.username
                self.game_winner = self.author
                winner_pk = self.author.pk
            else:
                self.game_winner = self.opponent
                winner = self.opponent.username
                winner_pk = self.opponent.pk
            self.status = constants.GAME_ENDED
            self.save()
            r.publish(channel_name, json.dumps({"game_id": self.pk, "game_status": "completed", "winner": winner,
                                                "winner_pk": winner_pk, "is_tie": game_round.is_tie}))
            return True
        return False

    class Meta:
        verbose_name = "Game's lobbie"
        verbose_name_plural = "Game's lobbies"
        ordering = ("created", )


class Round(BaseModel):
    """
    Round models
    """
    game = models.ForeignKey(GameLobbie, related_name="game_rounds", verbose_name="Game lobbie")
    winner = models.ForeignKey(User, verbose_name="won_rounds", null=True, blank=True)
    is_tie = models.BooleanField(default=False, verbose_name="Tie")
    round_number = models.PositiveIntegerField(verbose_name="Round number", default=1)
    user_hint = models.ForeignKey(User, verbose_name="User hint", related_name="round_hints", null=True, blank=True)

    def save(self, *args, **kwargs):
        """
        Overwrite save method to increase round_number
        """
        try:
            last_round = Round.objects.filter(game=self.game).latest('created')
            # if last round not self
            if last_round.pk != self.pk:
                self.round_number = last_round.round_number + 1
        except Round.DoesNotExist:
            pass
        if self.is_tie and self.winner:
            self.winner = None
        super(Round, self).save(*args, **kwargs)

    def __str__(self):
        return "Round {} of {}".format(self.round_number, self.game.title)

    class Meta:
        verbose_name = "Round"
        verbose_name_plural = "Rounds"
        ordering = ("created",)


class Party(BaseModel):
    """
    Party model
    """
    GAME_CHOICES = (
        (constants.ROCK, "rock"),
        (constants.SPOCK, "Spock"),
        (constants.PAPER, "paper"),
        (constants.LIZARD, "lizard"),
        (constants.SCISSORS, "scissors")
    )
    user = models.ForeignKey(User, related_name="parties", verbose_name="User")
    user_choice = models.PositiveIntegerField(choices=GAME_CHOICES, verbose_name="User choice")
    round = models.ForeignKey(Round, related_name="parties", verbose_name="Round")

    def __str__(self):
        return "User {} in game {} round {}".format(self.user.username, self.round.game.title, self.round.round_number)

    class Meta:
        verbose_name = "Party"
        verbose_name_plural = "Parties"
        ordering = ("created", )