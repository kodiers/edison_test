import json

from django.contrib.auth.models import User

from .models import Round


def publish_round_information(redis_connection, channel_name, game, old_round, timeout, winner=None, winner_pk=None):
    """
    Publish round information to redis channel and create new round.
    :param redis_connection: redis connection object
    :param channel_name: channel name
    :param game: GameLobbie object
    :param old_round: Round object: old round
    :param timeout: Bool: if round timeouted - True
    :param winner: str: if has winner - winner username
    :param winner_pk: int: if has winner - winner pk
    :return: None
    """
    new_round = Round()
    new_round.game = game
    new_round.save()
    redis_connection.publish(channel_name, json.dumps({"old_round": old_round.round_number, "round_id": new_round.pk,
                                                       "round": new_round.round_number, "game_id": game.pk,
                                                       "round_state": "completed", "game_status": "playing",
                                                       "winner": winner, "winner_pk": winner_pk,
                                                       "is_tie": old_round.is_tie, "is_timeout": timeout }))
