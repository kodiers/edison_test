from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^join_game/(?P<pk>\d+)/$', views.join_game, name="join_game"),
    url(r'^create_game/$', views.create_game, name="create_game"),
    url(r'^start_game/$', views.start_game, name='start_game'),
    url(r'^make_party/$', views.make_party, name='make_party'),
    url(r'^leave_game/$', views.leave_game, name='leave_game'),
    url(r'^get_hint/$', views.get_hint, name='get_hint'),
    url(r'^round_timeout/$', views.round_timeout, name='round_timeout'),
]