from django import forms

from .models import GameLobbie


class GameLobbieForm(forms.ModelForm):
    """
    Form for create GameLobbie
    """
    class Meta:
        model = GameLobbie
        fields = ("title", "party_time",)