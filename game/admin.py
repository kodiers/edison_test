from django.contrib import admin

from .models import GameLobbie, Party, Round

# Register your models here.


admin.site.register(GameLobbie)
admin.site.register(Party)
admin.site.register(Round)
