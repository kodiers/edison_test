
function getOnlineList() {
    $.get('http://edison.it-national.com/api/online-users/', {}, function (data, xhr, status) {
        if (status.status == 200) {
            $('#userlist').html('');
            for (var i = 0; i < data.length; i++) {
                $('#userlist').append("<p id='user" + data[i]['id'] + "'>" + data[i]['username'] + '</p>');
            }
        }
    });
}
function checkOnline(host) {
    var ws = new WebSocket(host);
    ws.onopen = function (e) {
        console.log("Connected");
    }
    ws.onclose = function (e) {
        console.log("Closed");
    }
    ws.onmessage = function (e) {
        var response = JSON.parse(e.data);
        if (response['error'])
            // Show error
            console.log(response);
        if (response['status'] == 'offline') {
            // If user offline - remove from list
            getOnlineList();
        }
        if (response['status'] == 'online') {
            console.log('online received');
            // if user online - get his username and add to list
            getOnlineList();

        }
    }
    window.onbeforeunload = function() {
        ws.close();
    }
}
$(document).ready(function () {
    getOnlineList();
});