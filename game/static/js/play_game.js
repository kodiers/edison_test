
function countdown(pk){
    // Show timer
    var n=$('.counter').attr('id');
    var c=n;
    $('.counter').text(c);
    var interval = setInterval(function () {
        c--;
        if (c > 0){
            $('.counter').text(c);
        }
        if(c == 0){
            // If timer end - end round
            $('#makeParty').prop("disabled", true);
            $('#party').prop("disabled", true);
            $('#getHint').prop("disabled", true);
            $.post('/game/round_timeout/', {pk: pk}, function (data) {
                console.log(data);
            });
            $('.counter').attr('id', n);
            console.log('timer end');
            clearInterval(interval);
            return;
        }
    }, 1000);
    return interval;
}
function playGame(username, author, count, game_pk, user_pk) {
    var host = "ws://edison.it-national.com:8888/game_start/" + game_pk +"/" + user_pk + "/";
    var ws = new WebSocket(host);
    ws.onopen = function (e) {
        console.log("Connected");
    }
    ws.onclose = function (e) {
        console.log("Closed");
    }
    ws.onmessage = function (e) {
        var response = JSON.parse(e.data);
        if (response['error']) {
            // log error
            console.log(response);
        }
        if (response['game_status'] == 'ready') {
            // if opponent joined the game - author can start game
            if (username != response['opponent'])
                $('#game_status').text("Your opponent " + response['opponent']);
            if (author == username)
                $('#game_status').append('<br><a href="#" class="btn btn-success" id="start_game" onclick="StartGame('+ game_pk + '); event.preventDefault();">Start game</a>');
        }
        if (response['game_status'] == "started") {
            // playing till timer out
            $('#start_game').remove();
            $('#game_status').text("Game started");
            $('#game_status').append("<h3 id='roundText'>Round #" + response['round'] + "</h3>");
            $('#round').val(response['round_id']);
            $('#makeParty').prop("disabled", false);
            $('#party').prop("disabled", false);
            $('#getHint').prop("disabled", false);
            countdown(response['round_id']);
        }
        if (response['round_state'] == 'make_party') {
            //
            $('#round').val(response['round_id']);
            $('#log').append("<h4>User: " + response['player'] + "make party</h4>");
        }
        if (response['round_state'] == 'completed') {
            // log rounds result
            if (response['is_timeout'] == true) {
                $('#log').append("<h5>Round #" + parseInt(response['old_round']) + " is timeout");
            } else if (response['is_tie'] == true) {
                $('#log').append("<h5>Round #" + parseInt(response['old_round']) + " is tie");
            } else {
                $('#log').append("<h5>Round #" + parseInt(response['old_round']) + " winner: " + response['winner']);
            }
            $('#round').val(response['round_id']);
            $('#roundText').text("Round #" + response['round']);
            $('#makeParty').prop("disabled", false);
            $('#party').prop("disabled", false);
            $('#getHint').prop("disabled", false);
            $('.counter').attr('id', count);
            $('.counter').text(count);
            countdown(response['round_id']);
        }
        if (response['game_status'] == 'hint_used') {
            console.log(response);
            $('#log').append(response['message']);
        }
        if (response['game_status'] == 'completed') {
            // end game
            alert("Game eneded! Winner: " + response['winner']);
            window.location.href =  "/";
        }
    }
}

function StartGame (game_pk) {
    // send ajax to start game
    $.post('/game/start_game/', {pk: game_pk}, function (data) {
        console.log(data);
    });
}

function makeParty(game_pk) {
    // make party
    var value = $('#party').val();
    var roundNum = $('#round').val();
    $('#makeParty').prop("disabled", true);
    $('#party').prop("disabled", true);
    $('#getHint').prop("disabled", true);
    $.post('/game/make_party/', {pk: game_pk, round: roundNum, choice: value}, function (data) {
        console.log(data);
    });
}

function getHint() {
    // get hint
    var roundNum = $('#round').val();
    $.post('/game/get_hint/', {round: roundNum}, function (data) {
        console.log(data);
        if (data['status'] == 'ok') {
            $('#hint').text(data['hint']);
        } else {
            $('#hint').text(data['error']);
        }
    });
}