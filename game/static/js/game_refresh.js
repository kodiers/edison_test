
function checkNewGames() {
    var ws = new WebSocket('ws://edison.it-national.com:8888/check_games/');
    ws.onopen = function (e) {
        console.log("Connected");
    }
    ws.onclose = function (e) {
        console.log("Closed");
    }
    ws.onmessage = function (e) {
        var response = JSON.parse(e.data);
        if (response['error'])
            // Show error
            console.log(response);
        if (response['status'] == 'new') {
            // If new game added - append to lisy
            $('#tableGames').append('<tr><td>' + response['title'] +'</td><td>'+ response['author'] +'</td><td>'+
                response['game_status'] +'</td><td><a href="/game/join_game/'+ response['pk'] + '/">Join game</a>' +'</td></tr>')
        }
    }
}