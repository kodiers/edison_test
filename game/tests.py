from django.test import TestCase
from django.contrib.auth.models import User

from .models import GameLobbie

# Create your tests here.


class TestGame(TestCase):
    def setUp(self):
        User.objects.create_user('user1', 'user1@mail.mm', 'p@ssw0rd')
        User.objects.create_user('user2', 'user2@mail.mm', 'p@ssw0rd')

    def test_create_game(self):
        self.client.login(username='user1', password='p@ssw0rd')
        response = self.client.post('/game/create_game/', {'title': 'test', 'party_time': 10})
        self.assertEquals(response.status_code, 302)

    def test_join_game(self):
        self.client.login(username='user1', password='p@ssw0rd')
        self.client.post('/game/create_game/', {'title': 'test', 'party_time': 10})
        game = GameLobbie.objects.last()
        self.client.login(username='user2', password='p@ssw0rd')
        response = self.client.get('/game/join_game/' + str(game.pk) + '/')
        self.assertContains(response, 'opponent')
